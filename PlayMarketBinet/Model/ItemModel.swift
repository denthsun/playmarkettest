//
//  ItemModel.swift
//  PlayMarketBinet
//
//  Created by Denis Velikanov on 20.09.2021.
//

import Foundation

struct ItemModel: Decodable {
    var name: String
    var version: String
    var time: Int
    var url: String
    var file: String
    var comment: String
}
