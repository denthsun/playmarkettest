//
//  DataFetcher.swift
//  BNET_Test
//
//  Created by Denis Velikanov on 08.09.2021.
//

import Foundation

class DataFetcher {
  
    let dataProvider = DataProvider()
    var networkDataFetcher: DataFetcherProtocol
    let apiUrlString = "http://bnet.i-partner.ru/projects/playMarket/?json=1"
    let dataGroup = DispatchGroup()
    
    func dataRequest(completion: @escaping ([ItemModel]?) -> Void) {
        dataGroup.enter()
        networkDataFetcher.fetchGETGenericData(urlString: apiUrlString, response: completion)
    }
    
    init(networkDataFetcher: DataFetcherProtocol = NetworkDataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
    }
}



