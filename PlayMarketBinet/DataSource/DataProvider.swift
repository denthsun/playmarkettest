//
//  DataProvider.swift
//  PlayMarketBinet
//
//  Created by Denis Velikanov on 20.09.2021.
//

import Foundation
import RxSwift
import RxCocoa

class DataProvider {
    
    var items = PublishSubject<[ItemModel]>()
    
    var itemsList: [ItemModel] = []
    
    func fetchItems() {
        items.onNext(itemsList)
        items.onCompleted()
    }
}
