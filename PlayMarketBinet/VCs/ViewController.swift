//
//  ViewController.swift
//  PlayMarketBinet
//
//  Created by Denis Velikanov on 20.09.2021.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire

final class ViewController: UIViewController {
    
    private let dataFetcher = DataFetcher()
    private let tableView = UITableView()
    private var bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setupUI()
        constraintUI()
        aMRequest()
     //   dataRequest()
     //   bindData()
        setGuestures()
    }
    
    func aMRequest() {
        
            AF.request("http://bnet.i-partner.ru/projects/playMarket/?json=1")
                .validate()
                .responseDecodable(of: [ItemModel].self) { [weak self] (response) in
                    guard let items = response.value else { return }
                    self?.dataFetcher.dataProvider.itemsList = items
                    self?.bindData()
                }
        
    }
    
    private func setupUI() {
        [tableView].forEach { view.addSubview($0) }
    }
    
    private func constraintUI() {
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor)
    }
    
    private func registerCell() {
        tableView.register(ItemCell.self, forCellReuseIdentifier: ItemCell.identifier)
    }
    
    private func dataRequest() {
        dataFetcher.dataRequest { [weak self] items in
            guard let currentItems = items else { return }
            self?.dataFetcher.dataProvider.itemsList = currentItems
            self?.dataFetcher.dataGroup.leave()
        }
    }
    
    private func bindData() {
        dataFetcher.dataGroup.notify(queue: DispatchQueue.global()) {
            DispatchQueue.main.async { [weak self] in
                self?.bindTableView()
            }
        }
    }
    
    private func secToDate(seconds: Int) -> String {
        let date = "\(Date(timeIntervalSince1970: TimeInterval(seconds)))"
        let dataFormatter = DateFormatter()
        
        return date
    }
    
    private func setGuestures() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        self.view.addGestureRecognizer(longPressRecognizer)
    }
    
    private func bindTableView() {
        
        dataFetcher.dataProvider.items.bind(to: tableView.rx.items(cellIdentifier: ItemCell.identifier, cellType: ItemCell.self)) { row, item, cell in
            
         
            let itemDate = self.secToDate(seconds: item.time)
            let labeledDate = itemDate.prefix(20)
            
            cell.nameLabel.text = item.name
            cell.versionLabel.text = item.version
            cell.dateLabel.text = "\(labeledDate)"
            
            
            DispatchQueue.main.async {
                cell.itemImage.image = UIImage(named: "cross")
            }
            
            //                guard let imageURL = URL(string: item.url) else { return }
            //
            //                if let data = try? Data(contentsOf: imageURL) {
            //                    DispatchQueue.main.async {
            //                        cell.itemImage.image = UIImage(data: data)
            //                    }
            //                }
                        
        }.disposed(by: bag)
        
        tableView.rx.modelSelected(ItemModel.self).bind { [weak self] item in
            
            let alert = UIAlertController(title: "Действие", message: "Что сделать?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Launch", style: .default, handler: { _ in
                // lauch function
                self?.lauch()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
                // cancel
                self?.cancelTapped()
            }))
            alert.addAction(UIAlertAction(title: "Download", style: .default, handler: { _ in
                // download app
                self?.downloadTapped()
                
            }))
            self?.present(alert, animated: true, completion: nil)
            
        }.disposed(by: bag)
        
        dataFetcher.dataProvider.fetchItems()
    }
    
    private func lauch() {
        print("lauch tapped")
    }
    
    private func cancelTapped() {
        print("cancel tapped")
    }
    
    private func downloadTapped() {
        print("download tapped")
    }
    
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        print("longpressed")
        
        
        let alert = UIAlertController(title: "Действие", message: "Что сделать?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            // delete function
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            // cancel
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
