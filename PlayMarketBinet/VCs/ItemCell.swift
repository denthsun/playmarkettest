//
//  ItemCell.swift
//  PlayMarketBinet
//
//  Created by Denis Velikanov on 20.09.2021.
//

import UIKit

final class ItemCell: UITableViewCell {

    static let identifier = "ItemCell"
    
    let nameLabel = UILabel()
    let versionLabel = UILabel()
    let dateLabel = UILabel()
    let itemImage = UIImageView()
    let devideView = UIView()

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    private func setupUI() {
        [itemImage, nameLabel, versionLabel, dateLabel, devideView].forEach { contentView.addSubview($0) }
        
        selectionStyle = .none
        
        nameLabel.font = UIFont.boldSystemFont(ofSize: 14)
        [versionLabel, dateLabel].forEach { $0.font = UIFont.systemFont(ofSize: 11) }
        [nameLabel, versionLabel, dateLabel].forEach { $0.adjustsFontSizeToFitWidth = true }
        
        itemImage.contentMode = .scaleToFill
        devideView.backgroundColor = .black
        devideView.heightAnchor.constraint(equalToConstant: 4).isActive = true
    }
    
    private func constraintUI() {
        itemImage.anchor(top: contentView.safeAreaLayoutGuide.topAnchor, leading: contentView.safeAreaLayoutGuide.leadingAnchor, bottom: contentView.safeAreaLayoutGuide.bottomAnchor, trailing: nameLabel.safeAreaLayoutGuide.leadingAnchor)
        nameLabel.anchor(top: contentView.safeAreaLayoutGuide.topAnchor, leading: itemImage.safeAreaLayoutGuide.trailingAnchor, bottom: versionLabel.safeAreaLayoutGuide.topAnchor, trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        versionLabel.anchor(top: nameLabel.safeAreaLayoutGuide.bottomAnchor, leading: itemImage.safeAreaLayoutGuide.trailingAnchor, bottom: devideView.safeAreaLayoutGuide.topAnchor, trailing: nil)
        dateLabel.anchor(top: nameLabel.safeAreaLayoutGuide.bottomAnchor, leading: nil,  bottom: devideView.safeAreaLayoutGuide.topAnchor, trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        devideView.anchor(top: versionLabel.safeAreaLayoutGuide.bottomAnchor, leading: contentView.safeAreaLayoutGuide.leadingAnchor, bottom: contentView.safeAreaLayoutGuide.bottomAnchor, trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        itemImage.widthAnchor.constraint(equalTo: itemImage.heightAnchor).isActive = true
        itemImage.heightAnchor.constraint(equalToConstant: contentView.layer.bounds.height - 10).isActive = true
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        constraintUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
