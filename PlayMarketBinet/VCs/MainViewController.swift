//
//  MainViewController.swift
//  PlayMarketBinet
//
//  Created by Denis Velikanov on 20.09.2021.
//

import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        title = "PlayMarket"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let item1 = ViewController()
        let icon1 = UITabBarItem(title: "Список", image: UIImage(systemName: "doc.plaintext"), selectedImage: UIImage(systemName: "doc.plaintext.fill"))
        item1.tabBarItem = icon1
        
        let item2 = HIstoryViewController()
        let icon2 = UITabBarItem(title: "История", image: UIImage(systemName: "arrowshape.turn.up.right"), selectedImage: UIImage(systemName: "arrowshape.turn.up.right.fill"))
        item2.tabBarItem = icon2
        
        let controllers = [item1, item2]
        self.viewControllers = controllers
        
    }
    

}
